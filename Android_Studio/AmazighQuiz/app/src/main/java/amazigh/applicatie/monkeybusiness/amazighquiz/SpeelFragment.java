package amazigh.applicatie.monkeybusiness.amazighquiz;

import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

public class SpeelFragment extends Fragment {

    static MediaPlayer mediaPlayer;
    static List<List<Map<String, String>>> gameData;
    static ArrayList<Integer> correctPositions;
    static Integer score;
    static Integer categoryCount;
    static Integer currentPage;

    int lives;
    int correctInt;
    int correctPos;
    String amaWord;
    List<Map<String, String>> currentPageData;

    ViewPagerActivity viewPagerActivity;
    GridView grid;
    TextView textView;
    TextView allCorrect;

    public SpeelFragment() {
        lives = 3;
        correctInt = 0;

        if (score == null ) {
            currentPage = 0;
            score = 0;
            correctPositions = new ArrayList<>();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewPagerActivity = (ViewPagerActivity) getActivity();

        if (gameData == null) {
            setGameData();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;

        if ((currentPage) != categoryCount) {

            currentPageData = gameData.get(currentPage);
            correctPos = correctPositions.get(currentPage);
            view = inflater.inflate(R.layout.activity_quizgridview, container, false);

            amaWord = currentPageData.get(correctPos).get("ama");
            textView = view.findViewById(R.id.wordTextView);
            textView.setText(amaWord);

            if (currentPage == 0) {
                playSound();
            }

            setAdapter(view);
        }  else {
            view = inflater.inflate(R.layout.activity_lastpageview, container, false);

            if ((categoryCount * 10 - 10) == score) {
                allCorrect = view.findViewById(R.id.allCorrect);
                allCorrect.setText(R.string.all_correct);
            }

            textView = view.findViewById(R.id.scoreTextView);
            textView.setText(score.toString());

            Button closeButton = view.findViewById(R.id.closeButton);
            closeButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    unInit();
                    getActivity().finish();
                }
            });
        }

        currentPage++;
        return view;
    }

    private void setAdapter(View view) {
        SpeelCardView speelCardView = null;
        speelCardView = new SpeelCardView(getActivity(), currentPageData);

        grid = view.findViewById(R.id.grid);

        grid.setAdapter(speelCardView);
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                if (position == correctPos) {
                    score = score + 10;

                    Toast.makeText(getActivity(), "Correct!!", Toast.LENGTH_SHORT).show();
//                    Toast.makeText(getActivity(), gameData.get(currentPage - 1).get(correctPositions.get(currentPage - 1)).get("snd"), Toast.LENGTH_SHORT).show();

                    playSound();
                    viewPagerActivity.setScore(score);
                    viewPagerActivity.nextPage();
                } else {
                    lives--;

                    if (lives > 0) {
                        Toast.makeText(getActivity(), "Verkeerd, nog " + lives + " kans(en)", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), "gefaald", Toast.LENGTH_SHORT).show();
                        playSound();
                        viewPagerActivity.nextPage();
                    }
                }
            }
        });
    }

    private ArrayList<Integer> generateNumbs(int categoryCount) {
        ArrayList<Integer> generatedNumbs = new ArrayList<>();

        while (generatedNumbs.size() != 6 ) {
            int randomNumb = ThreadLocalRandom.current().nextInt(0, categoryCount);
            Boolean exists = false;

            if (generatedNumbs.size() == 0) {
                correctInt = randomNumb;
            }

            for (int numb : generatedNumbs) {
                if (numb == randomNumb) {
                    exists = true;
                }
            }

            if (!exists) {
                generatedNumbs.add(randomNumb);
            }
        }

        Collections.sort(generatedNumbs);
        return generatedNumbs;
    }

    private void setGameData() {
        gameData = new ArrayList<>();

        try {
            String jsonString = GlobalFunctions.loadJSONFromAsset(getActivity(), R.raw.data);
            JSONObject json = new JSONObject(jsonString);
            JSONArray jsonCategoryData = json.getJSONObject("category").getJSONArray(getArguments().getString("category"));

            categoryCount = jsonCategoryData.length();

            for (int i = 0; i <= categoryCount; i ++) {
                ArrayList<Integer> generatedNumbs = generateNumbs(categoryCount);
                List<Map<String, String>> pageData = new ArrayList<>();

                for (int j = 0; j < generatedNumbs.size(); j++) {
                    if (generatedNumbs.get(j) == correctInt) {
                        correctPositions.add(j);
                    }
                    pageData.add(GlobalFunctions.getQuizData(jsonCategoryData, generatedNumbs.get(j)));
                }
                gameData.add(pageData);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void playSound() {
        if (mediaPlayer != null) {
            mediaPlayer.release();
        }

        mediaPlayer = new MediaPlayer();
        String sndFileName;

        if (currentPage.equals(0)) {
            sndFileName = gameData.get(currentPage).get(correctPositions.get(currentPage)).get("snd");
        }
        else if (currentPage - 1 != categoryCount) {
            sndFileName = gameData.get(currentPage - 1).get(correctPositions.get(currentPage - 1)).get("snd");
            Log.e("sound", Integer.toString(currentPage - 1));
        } else return;


        try {
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
            }

            AssetFileDescriptor descriptor = getContext().getAssets().openFd(sndFileName);
            mediaPlayer.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();

            mediaPlayer.prepare();
            mediaPlayer.setVolume(1f, 1f);
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void unInit() {
        mediaPlayer = null;
        gameData = null;
        correctPositions= null;
        score = null;
        categoryCount = null;
        currentPage = null;
    }
}