package amazigh.applicatie.monkeybusiness.amazighquiz;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

public final class SpeelCardView extends BaseAdapter {
    private Context mContext;
    private final List<Map<String, String>> gameData;
    private String  title;
    private String imgName;

    SpeelCardView(Context c, List<Map<String, String>> gameData) {
        mContext = c;
        this.gameData = gameData;
    }

    public int getCount() {
        return gameData.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View gridView;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        imgName = gameData.get(position).get("img");

        if (convertView == null) {
            gridView = new View(mContext);
            gridView = inflater.inflate(R.layout.quizcardview, null);
            View backgroundView = gridView.findViewById(R.id.backgroundView);
            View backgroundViewRootView = backgroundView.getRootView();
            ImageView imageView = gridView.findViewById(R.id.grid_image);

//            backgroundViewRootView.setBackgroundColor(mContext.getResources().getColor(R.color.red));
            try {
                InputStream is = mContext.getAssets().open(imgName);
                Bitmap bitmap = BitmapFactory.decodeStream(is);
                imageView.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else gridView = convertView;

        return gridView;
    }
}

