package amazigh.applicatie.monkeybusiness.amazighquiz;

import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.content.Context;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.view.View;
import java.io.IOException;
import java.io.InputStream;

public class OefenFragment extends Fragment {

    String category;
    JSONArray jsonCategoryData;
    String imagePath;
    String soundpath;
    String vertalingNL;
    String vertalingAmazigh;
    static Integer position = 0;
    static MediaPlayer mediaPlayer = new MediaPlayer();
    Context context;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        category = this.getArguments().getString("category");
        context = getActivity();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        GetJsonData();

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_oefenfragment, container, false);
        TextView textView1 = view.findViewById(R.id.textView1);
        TextView textView2 = view.findViewById(R.id.textView2);
        ImageView imageView1 = view.findViewById(R.id.imageButton);
        Button audiobutton = view.findViewById(R.id.audioButton);
        Button backButton = view.findViewById(R.id.Backbutton);

        vertalingNL = GlobalFunctions.getQuizData(jsonCategoryData, position).get("ned").toString();
        vertalingAmazigh = GlobalFunctions.getQuizData(jsonCategoryData, position).get("ama").toString();
        imagePath = GlobalFunctions.getQuizData(jsonCategoryData, position).get("img").toString();
        soundpath = GlobalFunctions.getQuizData(jsonCategoryData, position).get("snd").toString();
        position = position + 1;
        if (position > 14){
            position = 0;
        }

        //set image
        try {
            InputStream Stream = context.getAssets().open(imagePath);
            Bitmap bitmap = BitmapFactory.decodeStream(Stream);
            imageView1.setImageBitmap(bitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }
        audiobutton.setOnClickListener(eventListener);
        backButton.setOnClickListener(handler);

        textView1.setText(vertalingNL);
        textView2.setText(vertalingAmazigh);

        return view;
    }


    public void GetJsonData() {

        if (getActivity() != null) {
            try {
                String jsonString = GlobalFunctions.loadJSONFromAsset(getActivity(), R.raw.data);
                JSONObject json = new JSONObject(jsonString);
                jsonCategoryData = json.getJSONObject("category").getJSONArray(this.category);
                }
             catch (JSONException e) {
                e.printStackTrace();
            }
         }
    }

    public void playsound(String sndpath) throws IOException {
        if (mediaPlayer != null){
            mediaPlayer.release();
        }
        mediaPlayer = new MediaPlayer();

        if (mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = new MediaPlayer();
        }

        AssetFileDescriptor descriptor = getContext().getAssets().openFd(sndpath);
        mediaPlayer.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());

        mediaPlayer.prepare();
        mediaPlayer.setVolume(1f, 1f);
        mediaPlayer.start();


    }

    private View.OnClickListener eventListener = new View.OnClickListener() {
        public void onClick(View v) {
            try {
                playsound(soundpath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    };

    View.OnClickListener handler = new View.OnClickListener() {
        public void onClick(View v) {

            position = 0;
            mediaPlayer = null;
            getActivity().finish();
        }
    };
}
