package amazigh.applicatie.monkeybusiness.amazighquiz;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ViewPagerActivity extends FragmentActivity {

    String type;
    String category;
    JSONArray jsonCategoryData;
    int pageCount;

    private ViewPager viewPager;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        this.type = intent.getStringExtra("type");
        this.category = intent.getStringExtra("category");

        try {
            String jsonString = GlobalFunctions.loadJSONFromAsset(this, R.raw.data);
            JSONObject json = new JSONObject(jsonString);
            jsonCategoryData = json.getJSONObject("category").getJSONArray(category);

            pageCount = jsonCategoryData.length();

        }  catch (JSONException e) {
            e.printStackTrace();
        }

        if (type.equals("oefen")) {
            setContentView(R.layout.activity_viewpager);
        } else {

            setContentView(R.layout.activity_customviewpager);
            textView = findViewById(R.id.scoreTextView);
            textView.setText("0");

        }

        viewPager = findViewById(R.id.pager);
        PagerAdapter mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(mPagerAdapter);


    }

    @Override
    public void onBackPressed() {
        if (type.equals("oefen")) {
            if (viewPager.getCurrentItem() == 0) {
                // If the user is currently looking at the first step, allow the system to handle the
                // Back button. This calls finish() on this activity and pops the back stack.
                super.onBackPressed();
            } else {
                // Otherwise, select the previous step.
                viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
            }
        } else{
            SpeelFragment.score = 0;

            super.onBackPressed();
        }
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        private ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Bundle bundle = new Bundle();
            bundle.putString("category", category);
            Integer pos = position;

            if (type.equals("oefen")) {
                OefenFragment oefenFragment = new OefenFragment();
                oefenFragment.setArguments(bundle);

                return oefenFragment;

            } else {
                bundle.putInt("currentPage", pos);
                SpeelFragment speelFragment = new SpeelFragment();
                speelFragment.setArguments(bundle);
                return speelFragment;
            }
        }

        @Override
        public int getCount() {
            if (type.equals("oefen")) {
                return pageCount;
            } else {
                return pageCount + 1;
            }
        }
    }

    public void nextPage() {
        viewPager.setCurrentItem(getCurrentItem() + 1, true);
    }

    public void setScore(Integer score) {
        textView = findViewById(R.id.scoreTextView);
        textView.setText(score.toString());

        if (getCurrentItem() + 1 == pageCount - 1) {
            textView = findViewById(R.id.scoreTextView);
            textView.setText(" ");
        }
    }

    public Integer getCurrentItem() { return viewPager.getCurrentItem(); }
}