package amazigh.applicatie.monkeybusiness.amazighquiz;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

public class ScoreActivity extends AppCompatActivity {

    private Context context;
    public String jsonString;

    public String loadJSONFromAsset(Context context){
        String json = null;
        try {
            InputStream is = context.getAssets().open("score.json");

            int size = is.available();
            byte[] buffer = new byte[size];

            is.read(buffer);
            is.close();

            json = new String(buffer, "UTF-8");


        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        context = getApplicationContext();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);

        this.jsonString = loadJSONFromAsset(this.context);

        try {
            // voor nu score laten zijn voor wat het is.
            //ATM bestaat het json object hieruit moet de score gehaald worden
            JSONObject json = new JSONObject(this.jsonString);

            TextView textView = (TextView) findViewById(R.id. textViewscore);
            textView.setText(json.toString());


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }



}

