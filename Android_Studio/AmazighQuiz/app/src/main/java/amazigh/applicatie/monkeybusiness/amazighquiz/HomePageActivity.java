package amazigh.applicatie.monkeybusiness.amazighquiz;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class HomePageActivity extends AppCompatActivity {
    Button b1;
    Button b2;
    Button b3;
    Button b4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        b1 = (Button) findViewById(R.id.button1);
        b1.setOnClickListener(handler1);

        b2 = (Button) findViewById(R.id.button2);
        b2.setOnClickListener(handler2);

        b3 = (Button) findViewById(R.id.button3);
        b3.setOnClickListener(handler3);

        b4 = findViewById(R.id.button4);
        b4.setOnClickListener(handler4);
    }
    View.OnClickListener handler1 = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent1 = new Intent(HomePageActivity.this, CategoryListActivity.class);
            intent1.putExtra("type", "oefen");
            HomePageActivity.this.startActivity(intent1);
        }
    };

    View.OnClickListener handler2 = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent2 = new Intent (HomePageActivity.this, CategoryListActivity.class);
            intent2.putExtra("type", "speel");
            HomePageActivity.this.startActivity(intent2);
        }
    };

    View.OnClickListener handler3 = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent3 = new Intent (HomePageActivity.this, ScoreActivity.class);
            HomePageActivity.this.startActivity(intent3);
        }
    };

    View.OnClickListener handler4 = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent4 = new Intent (HomePageActivity.this, AboutActivity.class);
            HomePageActivity.this.startActivity(intent4);
        }
    };
}
