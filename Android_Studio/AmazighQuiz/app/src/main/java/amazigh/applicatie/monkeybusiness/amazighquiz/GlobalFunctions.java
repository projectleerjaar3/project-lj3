package amazigh.applicatie.monkeybusiness.amazighquiz;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class GlobalFunctions {
    Context mcontext;

    public GlobalFunctions(Context context) {
        this.mcontext = context;
    }


    public static String loadJSONFromAsset(Context context, int fileName) {
        String json = null;
        try {
            InputStream is = context.getResources().openRawResource(fileName);

            int size = is.available();
            byte[] buffer = new byte[size];

            is.read(buffer);
            is.close();

            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

public static Map getQuizData(JSONArray jsonArray, Integer numb) {

        try {
            JSONObject categoryItem = jsonArray.getJSONObject(numb);
            JSONObject categoryLang = categoryItem.getJSONObject("lang");

            String imagePath = categoryItem.getString("img");
            String soundPath = categoryItem.getString("snd");
            String translationNed = categoryLang.getString("ned");
            String translationAma = categoryLang.getString("ama");

            Map<String, String> keyValues = new HashMap<String, String>();
            keyValues.put("ned", translationNed);
            keyValues.put("ama", translationAma);
            keyValues.put("img", imagePath);
            keyValues.put("snd", soundPath);

            return keyValues;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ArrayList<String> getCategoryList(Context context) {
        ArrayList<String> catKeyStrings = new ArrayList<>();

        try {
            String jsonString = GlobalFunctions.loadJSONFromAsset(context, R.raw.data);
            JSONObject json = new JSONObject(jsonString);
            JSONObject categoryData = json.getJSONObject("category");
            Iterator categoryKeys = categoryData.keys();

            while (categoryKeys.hasNext()) {
                String key = (String) categoryKeys.next();
                catKeyStrings.add(key);
            }

            return catKeyStrings;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }
}
