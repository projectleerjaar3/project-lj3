package amazigh.applicatie.monkeybusiness.amazighquiz;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import org.json.JSONArray;

import java.util.ArrayList;

public class CategoryListActivity extends AppCompatActivity implements CategoryListAdapter.ItemClickListener{
    CategoryListAdapter adapter;
    String type;
    ArrayList<String> categoryList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        Intent intent = getIntent();
        type = intent.getStringExtra("type");

        // data to populate the RecyclerView with
        categoryList = GlobalFunctions.getCategoryList(this);

        // set up the RecyclerView
        RecyclerView recyclerView = findViewById(R.id.rvCategoryName);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new CategoryListAdapter(this, categoryList);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onItemClick(View view, int position) {
        Intent intent = new Intent (this, ViewPagerActivity.class);
        intent.putExtra("type", type);
        intent.putExtra("category", categoryList.get(position));
        startActivity(intent);
    }
}
